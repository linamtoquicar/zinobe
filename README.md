# zinobe

This is an app to create a country table and a calculations result table applying pandas functionality and persisting information into sqlite, mongodb and json_file.


## Installation guide
1. clone the repo: `git clone git@bitbucket.org:linamtoquicar/zinobe.git`
2. add to project an env file (.env) as example file .env.example
3. This project is configured to be executed in docker, so you have to build the container with: `docker-compose build`
4. To execute project just execute: `docker-compose up`

And that's it, the project will up an instance of mongodb and a service to will execute first unit tests and after will execute the app to generate and save the tables.

## TEST:
As follows you can find how it works:

1. when you execute `docker-compose up` you will see first the execution of unit test to prove that all is ok:
![alt test](static/sqlite.png)
2. after that you will find results table:
![alt table](static/table.png)
3. The app also saves into sqlite and mongodb two tables with result.
![alt sqlite_table](static/sqlite.png)
4. The app create a folder "json_files" where you can find the tables in json format.
