#!/usr/bin/env python
# -*- coding: utf-8 -*-
response_all_countries = """[{"name":"Afghanistan","topLevelDomain":[".af"],"alpha2Code":"AF","alpha3Code":"AFG","callingCodes":["93"],"capital":"Kabul","altSpellings":["AF","Af\u0121\u0101nist\u0101n"],"region":"Asia","subregion":"Southern Asia","population":26023100,"latlng":[33.0,65.0],"demonym":"Afghan","area":652230.0,"gini":27.8,"timezones":["UTC+04:30"],"borders":["IRN","PAK","TKM","UZB","TJK","CHN"],"nativeName":"\u0627\u0641\u063a\u0627\u0646\u0633\u062a\u0627\u0646","numericCode":"004","currencies":["AFN"],"languages":["ps","uz","tk"],"translations":{"de":"Afghanistan","es":"Afganist\xe1n","fr":"Afghanistan","ja":"\u30a2\u30d5\u30ac\u30cb\u30b9\u30bf\u30f3","it":"Afghanistan"},"relevance":"0"},{"name":"\xc5land Islands","topLevelDomain":[".ax"],"alpha2Code":"AX","alpha3Code":"ALA","callingCodes":["358"],"capital":"Mariehamn","altSpellings":["AX","Aaland","Aland","Ahvenanmaa"],"region":"Europe","subregion":"Northern Europe","population":28875,"latlng":[60.116667,19.9],"demonym":"\xc5landish","area":1580.0,"gini":null,"timezones":["UTC+02:00"],"borders":[],"nativeName":"\xc5land","numericCode":"248","currencies":["EUR"],"languages":["sv"],"translations":{"de":"\xc5land","es":"Alandia","fr":"\xc5land","ja":"\u30aa\u30fc\u30e9\u30f3\u30c9\u8af8\u5cf6","it":"Isole Aland"},"relevance":"0"}]"""


response_countries_by_region = '[{"name": "Albania", "topLevelDomain": [".al"], "alpha2Code": "AL", "alpha3Code": "ALB", "callingCodes": ["355"], "capital": "Tirana", "altSpellings": ["AL", "Shqip\\u00ebri", "Shqip\\u00ebria", "Shqipnia"], "region": "Europe", "subregion": "Southern Europe", "population": 2886026, "latlng": [41.0, 20.0], "demonym": "Albanian", "area": 28748.0, "gini": 34.5, "timezones": ["UTC+01:00"], "borders": ["MNE", "GRC", "MKD", "KOS"], "nativeName": "Shqip\\u00ebria", "numericCode": "008", "currencies": [{"code": "ALL", "name": "Albanian lek", "symbol": "L"}], "languages": [{"iso639_1": "sq", "iso639_2": "sqi", "name": "Albanian", "nativeName": "Shqip"}], "translations": {"de": "Albanien", "es": "Albania", "fr": "Albanie", "ja": "\\u30a2\\u30eb\\u30d0\\u30cb\\u30a2", "it": "Albania", "br": "Alb\\u00e2nia", "pt": "Alb\\u00e2nia", "nl": "Albani\\u00eb", "hr": "Albanija", "fa": "\\u0622\\u0644\\u0628\\u0627\\u0646\\u06cc"}, "flag": "https://restcountries.eu/data/alb.svg", "regionalBlocs": [{"acronym": "CEFTA", "name": "Central European Free Trade Agreement", "otherAcronyms": [], "otherNames": []}], "cioc": "ALB"}]'


response_country_by_name = """[
    {
        "name": "Albania",
        "topLevelDomain": [
            ".al"
        ],
        "alpha2Code": "AL",
        "alpha3Code": "ALB",
        "callingCodes": [
            "355"
        ],
        "capital": "Tirana",
        "altSpellings": [
            "AL",
            "Shqipëri",
            "Shqipëria",
            "Shqipnia"
        ],
        "region": "Europe",
        "subregion": "Southern Europe",
        "population": 2886026,
        "latlng": [
            41.0,
            20.0
        ],
        "demonym": "Albanian",
        "area": 28748.0,
        "gini": 34.5,
        "timezones": [
            "UTC+01:00"
        ],
        "borders": [
            "MNE",
            "GRC",
            "MKD",
            "KOS"
        ],
        "nativeName": "Shqipëria",
        "numericCode": "008",
        "currencies": [
            {
                "code": "ALL",
                "name": "Albanian lek",
                "symbol": "L"
            }
        ],
        "languages": [
            {
                "iso639_1": "sq",
                "iso639_2": "sqi",
                "name": "Albanian",
                "nativeName": "Shqip"
            }
        ],
        "translations": {
            "de": "Albanien",
            "es": "Albania",
            "fr": "Albanie",
            "ja": "アルバニア",
            "it": "Albania",
            "br": "Albânia",
            "pt": "Albânia",
            "nl": "Albanië",
            "hr": "Albanija",
            "fa": "آلبانی"
        },
        "flag": "https://restcountries.eu/data/alb.svg",
        "regionalBlocs": [
            {
                "acronym": "CEFTA",
                "name": "Central European Free Trade Agreement",
                "otherAcronyms": [],
                "otherNames": []
            }
        ],
        "cioc": "ALB"
    }
]"""
