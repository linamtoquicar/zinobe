import json

import numpy as np
import pandas as pd
from pandas.io.json import json_normalize

from clients import RegionClient, CountryClient
from controllers.table_controllers import TableController
from tests.mocks import response_countries_by_region, response_country_by_name


def test_generate_region_column_success(mocker):
    controller = TableController()
    client = mocker.patch.object(RegionClient, 'get_all_regions')
    client.return_value = np.array([u'Asia', u'Europe', u''], dtype=object)
    initial_table = controller.generate_region_column()
    expected_initial_table = pd.DataFrame(np.array([u'Asia', u'Europe', u''], dtype=object), columns=["Region"])
    assert initial_table.equals(expected_initial_table)


# def test_generate_region_column_error(mocker, region_url):
#     controller = TableController()
#     client = mocker.patch.object(RegionClient, 'get_all_regions')
#     client.return_value = None
#     initial_table = controller.generate_region_column()
#     assert initial_table == "It was not possible obtain regions."
#
#

def test_generate_column_country_name_success(mocker, country_url):
    controller = TableController()
    initial_table = pd.DataFrame(np.array([u'Europe', u''], dtype=object), columns=["Region"])
    client = mocker.patch.object(CountryClient, 'get_countries_of_region')
    client.return_value = json_normalize(json.loads(response_countries_by_region))
    time_client = mocker.patch.object(CountryClient, 'get_countries_by_region_response_time')
    time_client.return_value = '10'
    table = controller.generate_country_column(initial_table)
    expected_table = pd.DataFrame([["Europe", "Albania", "10"], ['', '', '']], columns=["Region", "City Name", "Time"])
    client.assert_called()
    assert table.equals(expected_table)


def test_generate_column_quantity_success(mocker, country_url):
    controller = TableController()
    initial_table = pd.DataFrame([["Europe", "Albania", "10"], ['', '', '']], columns=["Region", "City Name", "Time"])
    client = mocker.patch.object(CountryClient, 'get_countries_by_name')
    client.return_value = json_normalize(json.loads(response_country_by_name))
    time_client = mocker.patch.object(CountryClient, 'get_countries_by_name_response_time')
    time_client.return_value = '20'
    table = controller.generate_language_column(initial_table)
    expected_table = pd.DataFrame(
        [["Europe", "Albania", float("30"), "9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6"],
         ['', '', '', '']],
        columns=["Region", "City Name", "Time", "Language"])
    client.assert_called()
    assert table.equals(expected_table)


def test_generate_result_table(mocker):
    controller = TableController()
    client = mocker.patch.object(RegionClient, 'get_all_regions')
    client.return_value = np.array([u'Asia', u'Europe', u''], dtype=object)
    client_country = mocker.patch.object(CountryClient, 'get_countries_of_region')
    client_country.return_value = json_normalize(json.loads(response_countries_by_region))
    client_name = mocker.patch.object(CountryClient, 'get_countries_by_name')
    client_name.return_value = json_normalize(json.loads(response_country_by_name))
    controler_generate = mocker.patch.object(TableController, 'generate_language_column')
    controler_generate.return_value = pd.DataFrame(
        [["Europe", "Albania", float("30"), "9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6"],
         ['', '', '', '']],
        columns=["Region", "City Name", "Time", "Language"])
    response_table = controller.generate_result_table()

    print(response_table)
    expected_table = pd.DataFrame(
        [["Europe", "Albania", "9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6", float("30")],
         ['', '', '', '']],
        columns=["Region", "City Name", "Language", "Time"])
    assert response_table.equals(expected_table)
