import pandas as pd

from controllers.result_table_controllers import ResultTableController


def test_sum_time():
    table = pd.DataFrame(
        [["Europe", "Albania", "9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6", float("30")],
         ['Americas', 'Colombia', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', float("20")],
         ['Asia', 'Brasil', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', "30"]],
        columns=["Region", "City Name", "Language", "Time"])
    controller = ResultTableController(response_table=table)
    assert controller.get_sum_total_time() == 80


def test_mean_time():
    table = pd.DataFrame(
        [["Europe", "Albania", "9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6", float("30")],
         ['Americas', 'Colombia', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', float("20")],
         ['Asia', 'Brasil', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', "30"]],
        columns=["Region", "City Name", "Language", "Time"])
    controller = ResultTableController(response_table=table)
    assert controller.get_mean_time() == 26.666666666666668


def test_min_time():
    table = pd.DataFrame(
        [["Europe", "Albania", "9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6", float("30")],
         ['Americas', 'Colombia', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', float("20")],
         ['Asia', 'Brasil', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', "10"]],
        columns=["Region", "City Name", "Language", "Time"])
    controller = ResultTableController(response_table=table)
    assert controller.get_min_time() == 10


def test_max_time():
    table = pd.DataFrame(
        [["Europe", "Albania", "9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6", float("30")],
         ['Americas', 'Colombia', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', float("20")],
         ['Asia', 'Brasil', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', "10"]],
        columns=["Region", "City Name", "Language", "Time"])
    controller = ResultTableController(response_table=table)
    assert controller.get_max_time() == 30


def test_generate_result_table():
    table = pd.DataFrame(
        [["Europe", "Albania", "9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6", float("30")],
         ['Americas', 'Colombia', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', float("20")],
         ['Asia', 'Brasil', '9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6', "10"]],
        columns=["Region", "City Name", "Language", "Time"])
    controller = ResultTableController(response_table=table)
    result_table = controller.generate_result_table()
    expected_table = pd.DataFrame(
        [["sum total", 60.0],
         ["mean", 20.0],
         ["min", 10.0],
         ["max", 30.0]
         ],
        columns=["Description", "Value"])

    assert result_table.equals(expected_table)
