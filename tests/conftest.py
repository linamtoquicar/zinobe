import pytest


@pytest.fixture
def region_url():
    return "https://restcountries-v1.p.rapidapi.com/all"


@pytest.fixture
def country_url():
    return "https://restcountries.eu/rest/v2/"
