#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import os

import numpy as np
import requests
from pandas.io.json import json_normalize

from clients import RegionClient, CountryClient
from tests.mocks import response_all_countries, response_countries_by_region, response_country_by_name


def test_unauthorized_request_countries():
    response = requests.get(url="https://restcountries-v1.p.rapidapi.com/all")
    assert response.status_code == 401


def test_success_request_countries():
    response = requests.get(url="https://restcountries-v1.p.rapidapi.com/all", headers={"X-RapidAPI-Key": os.environ['RAPIDAPI_KEY']})
    assert response.status_code == 200


def test_api_get_country():
    response = requests.get(url="{}region/europe".format("https://restcountries.eu/rest/v2/"))
    assert response.status_code == 200
    assert response.json()


def test_get_all_regions_success(requests_mock, region_url):
    region_client = RegionClient()
    requests_mock.get(region_url, text=response_all_countries, status_code=200)

    regions = region_client.get_all_regions()
    np.testing.assert_array_equal(regions, np.array([u'Asia', u'Europe'], dtype=object))


def test_get_all_regions_failed(requests_mock, region_url):
    region_client = RegionClient()
    requests_mock.get(region_url, text=None, status_code=401)
    regions = region_client.get_all_regions()
    assert regions is None


def test_get_one_country_of_region_success(requests_mock, country_url):
    country_client = CountryClient()
    requests_mock.get("{}region/europe".format(country_url), text=response_countries_by_region, status_code=200)
    countries_of_region = country_client.get_countries_of_region(region="europe")
    assert countries_of_region.equals(json_normalize(json.loads(response_countries_by_region)))


def test_get_one_country_of_region_failed(requests_mock, country_url):
    country_client = CountryClient()
    requests_mock.get("{}region/europe".format(country_url), text=None, status_code=500)
    countries_of_region = country_client.get_countries_of_region(region="europe")
    assert countries_of_region is None


def test_get_one_country_by_name_success(requests_mock, country_url):
    country_client = CountryClient()
    requests_mock.get("{}name/albania".format(country_url), text=response_country_by_name, status_code=200)
    countries_of_region = country_client.get_countries_by_name(name="Albania")
    assert countries_of_region.equals(json_normalize(json.loads(response_countries_by_region)))


def test_get_one_country_by_name_failed(requests_mock, country_url):
    country_client = CountryClient()
    requests_mock.get("{}name/albania".format(country_url), text=None, status_code=500)
    countries_of_region = country_client.get_countries_by_name(name="Albania")
    assert countries_of_region is None
