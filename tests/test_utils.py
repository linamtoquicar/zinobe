from utils import Utility


def test_encrypt_sha():
    sha_encrypted = Utility.encrypt_string("Albanian")
    assert sha_encrypted == "9d0367457cdde38257789832edce1d1d835db99aa3b6c2a628b5124d8bd4ced6"
