import pandas as pd

from controllers.persistence_controller import PersistenceController
from controllers.result_table_controllers import ResultTableController
from controllers.table_controllers import TableController
from models.models import DbSqlite, JsonFile, MongoDB

if __name__ == "__main__":
    """
    This is the main executable, which shows result into console.
    This file works as a view.
    """
    controller = TableController()
    country_table = controller.generate_result_table()
    pd.options.display.max_columns = None
    pd.options.display.width = None

    print("Country Table")
    print(country_table)

    time_table_controller = ResultTableController(country_table)
    time_result_table = time_table_controller.generate_result_table()
    print("Time Result Table")
    print(time_result_table)

    saving_controller = PersistenceController(country_table, time_result_table)
    saving_controller.save_result_tables()

