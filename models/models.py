import json
import os
import sqlite3

from pymongo import MongoClient


class DbSqlite:
    def __init__(self):
        self.connection = sqlite3.connect(os.environ['DB_SQLITE_NAME'])
        self.cursor = self.connection.cursor()

    def save_table(self, table, table_name):
        sqlite_clean_table = "DROP TABLE IF EXISTS  {}; ".format(table_name)
        self.cursor.execute(sqlite_clean_table)
        table.to_sql(name=table_name, con=self.connection)

    def get_table(self, table_name):
        self.cursor.execute("SELECT * FROM {}".format(table_name))
        rows = self.cursor.fetchall()
        for row in rows:
            print(row)


class JsonFile:
    def __init__(self):
        self.path = "json_files"
        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def save_table_as_json(self, table, json_name):
        table.to_json(f"{self.path}/{json_name}")


class MongoDB:
    def __init__(self):
        client = MongoClient(
            os.environ['DB_PORT_27017_TCP_ADDR'],
            27017)
        self.db = client["zinobe"]

    def save_result_table(self, table, collection_name):
        collection = self.db[collection_name]
        df_json = table.T.to_json()
        df_json_list = json.loads(df_json).values()
        collection.insert(df_json_list)
