FROM python:3.7
ENV CODE=/code

COPY requirements.txt /
RUN pip install pip --upgrade
RUN pip install -r requirements.txt
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install -y sqlite3 libsqlite3-dev
RUN mkdir /db
RUN /usr/bin/sqlite3 /db/zinobe.db


WORKDIR $CODE
