import os

import requests
from pandas.io.json import json_normalize


class RegionClient:
    def __init__(self):
        self.url = "https://restcountries-v1.p.rapidapi.com/all"
        self.rapidapi_key = os.environ["RAPIDAPI_KEY"]

    def get_all_regions(self):
        all_countries = requests.get(url=self.url, headers={"X-RapidAPI-Key": self.rapidapi_key})
        if all_countries.status_code == 200:
            all_countries = all_countries.json()
            return json_normalize(all_countries)["region"].unique()


class CountryClient:
    def __init__(self):

        self.url = None
        self.country_by_region_response_time = None
        self.country_by_name_response_time = None

    def get_countries_of_region(self, region):
        region = region.lower()
        self.url = "https://restcountries.eu/rest/v2/region/{}".format(region.lower())
        all_countries_by_region = requests.get(url=self.url)
        if all_countries_by_region.status_code == 200:
            all_countries_by_region_json = all_countries_by_region.json()
            self.country_by_region_response_time = all_countries_by_region.elapsed.total_seconds()
            return json_normalize(all_countries_by_region_json)

    def get_countries_by_region_response_time(self):
        return self.country_by_region_response_time

    def get_countries_by_name(self, name):
        self.url = "https://restcountries.eu/rest/v2/name/{}".format(name.lower())
        country_response = requests.get(url=self.url)
        if country_response.status_code == 200:
            country_response_json = country_response.json()
            self.country_by_name_response_time = country_response.elapsed.total_seconds()
            return json_normalize(country_response_json)

    def get_countries_by_name_response_time(self):
        return self.country_by_name_response_time
