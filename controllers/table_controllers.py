import pandas as pd

from clients import RegionClient, CountryClient
from utils import Utility


class TableController:
    """
    TableController is a class in charge to communicate with clients to get information and
    generate a table with it.
    """
    def generate_result_table(self):
        region_column = self.generate_region_column()
        country_column = self.generate_country_column(region_column)
        language_column = self.generate_language_column(country_column)
        column_titles = ['Region', 'City Name', 'Language', 'Time']
        result_table = language_column.reindex(columns=column_titles)
        return result_table

    def generate_region_column(self):
        region_client = RegionClient()
        regions = region_client.get_all_regions()
        if regions is not None:
            initial_table = pd.DataFrame(regions, columns=["Region"])
            return initial_table

    def _generate_one_country(self, row):
        region = row['Region']
        if region:
            country_client = CountryClient()
            countries_by_region = country_client.get_countries_of_region(region=region)
            one_country = countries_by_region.sample(1)
            row['City Name'] = list(one_country['name'])[0]
            row['Time'] = country_client.get_countries_by_region_response_time()
        else:
            row['City Name'] = ''
            row['Time'] = ''
        return row

    def generate_country_column(self, initial_table):
        response_table = initial_table.apply(lambda x: self._generate_one_country(x), axis=1)
        return response_table

    def _get_language(self, row):
        country_name = row['City Name']
        if country_name:
            country_client = CountryClient()
            country_by_name = country_client.get_countries_by_name(name=country_name.lower())
            language_country = country_by_name.iloc[0].languages[0].get("name")
            row['Language'] = Utility.encrypt_string(language_country)
            row['Time'] = float(row['Time']) + float(country_client.get_countries_by_name_response_time())
        else:
            row['Language'] = ''
        return row

    def generate_language_column(self, table):
        response_table = table.apply(lambda x: self._get_language(x), axis=1)
        return response_table
