import pandas as pd


class ResultTableController:
    def __init__(self, response_table):
        """
        ResultTableController is a class in charge to make calculations and generate a table
        with the results.
        Args:
            response_table (DataFrame):  This is the table to obtain column time and make all
            calculations.
        """
        self.response_table = response_table

    def generate_result_table(self):
        return pd.DataFrame(
            [["sum total", self.get_sum_total_time()],
             ["mean", self.get_mean_time()],
             ["min", self.get_min_time()],
             ["max", self.get_max_time()]
             ],
            columns=["Description", "Value"])

    def get_sum_total_time(self):
        return pd.to_numeric(self.response_table['Time']).sum()

    def get_mean_time(self):
        return pd.to_numeric(self.response_table['Time']).mean()

    def get_min_time(self):
        return pd.to_numeric(self.response_table['Time']).min()

    def get_max_time(self):
        return pd.to_numeric(self.response_table['Time']).max()

    # def save_insto_sqlite(self):
    #     pass
    #
    # def generate_json_result_table(self):
    #     pass
    #
    # def save_into_mongo(self):
    #     pass
