from models.models import DbSqlite, MongoDB, JsonFile


class PersistenceController:
    def __init__(self, countries_table, result_table):
        """
        PersistenceController.
        This class communicates with models to save the tables in diferent ways.
        Args:
            countries_table(dataframe): Table generated with countries information
            result_table(dataframe): Table generated with time results values (mean, sum,
            max, min)
        """
        self.countries_table = countries_table
        self.time_result_table = result_table

    def save_result_tables(self):
        self._save_as_json_file()
        self._save_into_sqlite()
        self._save_into_mongo()

    def _save_into_sqlite(self):
        # save into db sqlite
        sqlite_db = DbSqlite()
        sqlite_db.save_table(self.countries_table, "country_table")
        sqlite_db.save_table(self.time_result_table, "time_result")
        print("tables saved into sqliteDB")

    def _save_into_mongo(self):
        # save in mongo
        mongo_db = MongoDB()
        mongo_db.save_result_table(self.countries_table, "country_collection")
        mongo_db.save_result_table(self.time_result_table, "time_result")
        print("Tables saved in mongo")

    def _save_as_json_file(self):
        # generate as json file
        json_file = JsonFile()
        json_file.save_table_as_json(self.countries_table, "country_table.json")
        json_file.save_table_as_json(self.time_result_table, "time_result.json")
        print("tables saved as .json")
